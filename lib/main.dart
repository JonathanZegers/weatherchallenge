import 'package:flutter/material.dart';
import 'package:weatherforcast/forecast/app_bar.dart';
import 'package:weatherforcast/forecast/background/background_with_rings.dart';
import 'package:weatherforcast/forecast/week_drawer.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Weather',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  OpenableController openableController;

  @override
  void initState() {
    super.initState();

    openableController = OpenableController(
      vsync: this,
      openDuration: const Duration(milliseconds: 250),
    )..addListener(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          BackgroundWithRings(),
          Positioned(
            top: 0.0,
            left: 0.0,
            right: 0.0,
            child: ForcastAppBar(
              onDrawerArrowTap: openableController.open,
            ),
          ),
          Stack(
            children: <Widget>[
              GestureDetector(
                onTap: openableController.isOpen()
                    ? openableController.close
                    : null,
              ),
              Transform(
                transform: Matrix4.translationValues(
                  125.0 * (1.0 - openableController.percentOpen),
                  0.0,
                  0.0,
                ),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: WeekDrawer(
                    onDaySelected: (String title) {
                      openableController.close();
                    },
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class OpenableController extends ChangeNotifier {
  OpenableState _state = OpenableState.closed;
  AnimationController _opening;

  OpenableController({
    @required TickerProvider vsync,
    @required Duration openDuration,
  }) : _opening = AnimationController(duration: openDuration, vsync: vsync) {
    _opening
      ..addListener(notifyListeners)
      ..addStatusListener((AnimationStatus status) {
        switch (status) {
          case AnimationStatus.forward:
            _state = OpenableState.opening;
            break;
          case AnimationStatus.completed:
            _state = OpenableState.open;

            break;
          case AnimationStatus.reverse:
            _state = OpenableState.closing;

            break;
          case AnimationStatus.dismissed:
            _state = OpenableState.closed;

            break;
        }
        notifyListeners();
      });
  }

  get state => _state;

  get percentOpen => _opening.value;

  bool isOpen() {
    return _state == OpenableState.open;
  }

  bool isOpening() {
    return _state == OpenableState.opening;
  }

  bool isClosed() {
    return _state == OpenableState.closed;
  }

  bool isClosing() {
    return _state == OpenableState.closing;
  }

  void open() {
    _opening.forward();
  }

  void close() {
    _opening.reverse();
  }

  void toggle() {
    if (isClosed()) {
      open();
    } else if (isOpen()) {
      close();
    }
  }
}

enum OpenableState {
  closed,
  opening,
  open,
  closing,
}
